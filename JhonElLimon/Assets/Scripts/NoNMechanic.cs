using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoNMechanic : MonoBehaviour
{
    public GameObject enemys;
    public GameObject objecto;
    public Transform[] points;

    private bool times = true;

    public float curva;


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            enemys.SetActive(false);
            times = false;
            objecto.transform.position = points[Random.Range(0, points.Length)].position;
        }
    }

    public void Update()
    { 
        if (times== false)
        {
            curva += Time.deltaTime;
            if (curva >= 3f)
            {
                enemys.SetActive(true);

                curva = 0f;
                times = true;
            }

        }
       
    }

    private void Start()
    {
        objecto.transform.position = points[Random.Range(0, points.Length)].position;
    }
}
